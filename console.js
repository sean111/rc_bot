exports.Console = Console;

var util = require('util');

function Console() {
    var self = this;
    process.stdin.resume();
    process.stdin.setEncoding('utf8');
    process.stdin.on('data', function(data) {
        var text = {};
        if(data.charAt(0) == '/') {
            var tmp = data.split(" ");
            text.command = tmp[0].replace("/", '').trimRight();
            tmp.shift();
            text.args = tmp;
            self.emit('command', text);
        }
    });

    process.EventEmitter.call(this);
}

util.inherits(Console, process.EventEmitter);