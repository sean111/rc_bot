var irc = require('./irc');
var console = require('./console');
var util = require('util');
var repl = require('repl');
var fs = require('fs');

var file = 'config.json';

var config = fs.readFileSync(file, 'utf8');
config = JSON.parse(config);

var client = new irc.IRCClient(config.server, config.port, config.nickname);
client.options.password = config.password;
client.connect();
client.addListener('end_motd', function() {
    if(typeof config.channels == "object") {
        config.channels.forEach(function(x) {
            client.join(x);
        });
    }
    else {
        client.join(config.channels);
    }
});

client.addListener('msg_received', function(from, to, message) {
    //Check for user links, need to add op check when I add in channel based OP list. Also add permited user list for allowing a timed window for a user to post a link
    var channelOps = client.getOps(to);
    var regex1 = message.match(/(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim);
    var regex2 = message.match(/(^|[^\/])(www\.[\S]+(\b|$))/gim);
    if((regex1 !== null || regex2 !== null) && channelOps.indexOf(from) == -1) {
        client.delMessage(from, to);
        client.speak(to, from+": Please do no post links in the chat");
    }

});

client.addListener("disconnected", function() {
    process.exit();
});

var command = new console.Console();

command.addListener('command', function(data) {
    //util.log("Recieved command "+data.command);
    var message = "";
    switch(data.command.toLowerCase()) {
        case "quit":
            if(data.args[1] === null) {
                message = "Requested from command line";
            }
            else {
                message = data.args.join(" ");
            }
            util.log("Quit msg "+message);
            client.quit(message);
        break;
        case "join":
            if(typeof data.args[0] !== undefined) {
                client.join(data.args[0]);
            }
        break;
        case "part":
            if(typeof data.args[0] !== undefined) {
                client.part(data.args[0]);
            }
        break;
        case "say":
            var target = data.args[0];
            data.args.shift();
            message = data.args.join(" ");
            client.speak(target, message);
        break;
        case "help":
            util.log(" >> There is no helping you.");
        break;
        case "ops":
            var channel = data.args[0];
            if(channel.indexOf('#')==-1) {
                channel = '#'+channel;
            }
            var ops = client.getOps(channel.trim());
            util.log(ops.toString());
        break;
    }
});
