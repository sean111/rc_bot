//This is loosely based off of node-irc: https://github.com/martynsmith/node-irc/

exports.IRCClient = IRCClient;

var net = require('net');
var util = require('util');
var codes = require('./codes');

function IRCClient(host, port, nickname) {
    var self = this;
    self.options = {
        server: host,
        port: port,
        nick: nickname,
        password: null
    };
    self.channelOps = [];
    self.addListener('msg', function(msg) {
        util.log(msg.command+": "+msg.args.join(" "));
        switch(msg.command) {
            case "PING":
                self.send("PONG "+msg.args[0]);
            break;
            case "NOTICE":

            break;
            case "rpl_endofmotd":
            case "err_nomotd":
                self.emit('end_motd');
            break;
            case "PRIVMSG":
                util.log("["+msg.args[0]+"] "+msg.nick+": "+msg.args[1]);
                self.emit('msg_received', msg.nick, msg.args[0], msg.args[1]);
            break;
            case "MODE":
                util.log(msg.args.join(" "));
                if(msg.args[1] == "+o") {
                    util.log("["+msg.args[0]+"]ADD OP "+msg.args[2]);
                    self.channelOps[msg.args[0]].push(msg.args[2]);
                    self.channelOps.forEach(function(value, index) {
                        util.format("Value: %s Index: $d", value, index);
                    });
                }
                else if(msg.args[1] == "-o") {
                    util.log("DEL OP "+msg.args[2]);
                }
            break;
            case "JOIN":
                if(msg.nick == self.options.nick) {
                    self.channelOps.push(msg.args[0]);
                    self.channelOps[msg.args[0]] = [];
                    util.log("I joined "+msg.args[0]);
                }
            break;
            case "PART":
                if(msg.nick == self.options.nick) {
                    delete self.channelOps[msg.args[0]];
                    util.log("I left "+msg.args[0]);
                }
            break;
        }
    });
    process.EventEmitter.call(this);
}

util.inherits(IRCClient, process.EventEmitter);

IRCClient.prototype.connect = function(callback) {
    var self = this;
    if(typeof(callback) == 'function') {
        self.once('registered', callback);
    }

    util.log('Server: '+self.options.server);
    self.conn = net.createConnection(self.options.port, self.options.server);
    self.conn.setTimeout(0);
    self.conn.setEncoding('utf8');
    self.conn.addListener('connect', function() {
        util.log('Connected');
        if(self.options.password !== null) {
            self.send("PASS "+self.options.password);
        }
        self.send("NICK "+self.options.nick);
        self.send("USER rc_bot 8 * rc_bot");
    });

    var buffer = '';
    self.conn.addListener('data', function(data) {
        buffer += data; //Brings the data together so it can be split apart by lines
        lines = buffer.split("\r\n");
        buffer = lines.pop();
        lines.forEach(function(line) {
            self.emit('msg', parseMessage(line, false));
        });
    });

    self.conn.addListener('end', function() {
        util.log('Connection closed');
        self.emit('disconnected');
    });

    self.conn.addListener('error', function(exception) {
        util.log('Error: '+exception);
    });
};

IRCClient.prototype.send = function(string) {
    util.log("Sending: "+string);
    this.conn.write(string+"\r\n");
};

IRCClient.prototype.join = function(channel) {
    if(channel.indexOf("#")==-1) {
        channel = "#"+channel;
    }
    util.log("Joining "+channel);
    this.send("JOIN "+channel);
};

IRCClient.prototype.part = function(channel) {
    if(channel.indexOf("#")==-1) {
        channel = "#"+channel;
    }
    util.log("Leaving "+channel);
    this.send("PART "+channel);
};

IRCClient.prototype.quit = function(message) {
    if(message === null) {
        message = "rc_bot has been terminated";
    }
    this.send("QUIT "+message);
};

IRCClient.prototype.speak = function(target, message) {
    this.send("PRIVMSG "+target+" :"+message);
};

IRCClient.prototype.delMessage = function(target, channel) {
    if(typeof channel === undefined || typeof target === undefined) { //I need to stop being lazy and start adding in variable validation
        return false;
    }
    if(channel.indexOf("#") == -1) {
        channel = '#'+channel;
    }
    util.log("Removing link from "+target+" in "+channel);
    this.send("PRIVMSG "+channel+" .timeout 1 "+target);
    return true;
};

IRCClient.prototype.getOps = function(channel) {
    var self = this;
    return self.channelOps[channel];
};

//Taken directly from node-irc
function parseMessage(line, stripColors) {
    var message = {};
    var match;

    if (stripColors) {
        line = line.replace(/[\x02\x1f\x16\x0f]|\x03\d{0,2}(?:,\d{0,2})?/g, "");
    }

    // Parse prefix
    if ( match = line.match(/^:([^ ]+) +/) ) {
        message.prefix = match[1];
        line = line.replace(/^:[^ ]+ +/, '');
        if ( match = message.prefix.match(/^([_a-zA-Z0-9\[\]\\`^{}|-]*)(!([^@]+)@(.*))?$/) ) {
            message.nick = match[1];
            message.user = match[3];
            message.host = match[4];
        }
        else {
            message.server = message.prefix;
        }
    }

    // Parse command
    match = line.match(/^([^ ]+) */);
    message.command = match[1];
    message.rawCommand = match[1];
    message.commandType = 'normal';
    line = line.replace(/^[^ ]+ +/, '');

    if ( codes[message.rawCommand] ) {
        message.command = codes[message.rawCommand].name;
        message.commandType = codes[message.rawCommand].type;
    }

    message.args = [];
    var middle, trailing;

    // Parse parameters
    if ( line.indexOf(':') != -1 ) {
        match = line.match(/(.*)(?:^:|\s+:)(.*)/);
        middle = match[1].trimRight();
        trailing = match[2];
    }
    else {
        middle = line;
    }

    if ( middle.length )
        message.args = middle.split(/ +/);

    if ( typeof(trailing) != 'undefined' && trailing.length )
        message.args.push(trailing);

    return message;
}
